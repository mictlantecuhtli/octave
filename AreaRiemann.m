%Autor: Fátima Azucena MC
%Fecha: 24_07_2023.
%Descripcion:  Grafique mediante sumas de riemman 
%el area bajo la curva de la funcion: 
%f(x)= x^2
%en un intervalo cerrado de [0, 1].

% Limpia la pantalla y las variables.
clc, clear
suma=0;

% Define los intervalos y el numero de rectangulos.
a = -5;
b = 1;
n = 100;
x=a

% Determina la longitud de la base o el incremento 

h = ((b-a)/n);

% Ciclo for para calcular la suma de cada rectangulo

for i=0:n,
    x += h;
    f = (x.^2);
    suma = suma + abs(h*f);
endfor    

disp(["El área bajo la curva es: ", num2str(suma)])

% Estable    la    linea   que   forma  la funcion
x2 = linspace(0,1,100);
y2 = (x.^2);

% Inicio del  intervalo, fin del intervalo, numero 
%de rectangulos.
xa = linspace(0,1,100);
yab = (x.^3);

% Dibuja una  grafica de barras una a lado de otra 
%sin espacios
g = bar(xa,yab,'histc');

% Permite continuar  graficando  despues de que ya 
%exista un grafica trazada
hold on

% Grafica la funcion
plot(x2,y2);

% Ajusta la grafica a los datos.
axis tight

