% Title        :Inecuacione
% Description  :Script para demostrar los tipos de axiomas
% Author       :Fátima Azucena MC
% Date         :11/02/2023

disp('')
disp('Inecuación de segundo grado con una incógnita')
disp('Resuelve la siguiente inecuación:')
disp('')
disp('x^2 - x > 12')

syms x
y = expand ((x^2 - ( 2 * x ) - 3) >= 0 );
p = solve ((x^2 - ( 2 * x ) - 3) >= 0);
disp('')
disp(y)
disp(' ')
disp(p)
disp('')
disp('By Honey (っ＾▿＾)')
