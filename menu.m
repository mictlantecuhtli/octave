% Title        :Desigualdades
% Description  :Script para resolver 3 tipos de desigualdades
% Author       :Fátima Azucena MC
% Date         :11/02/2023

% Limpiar
clear

function p = menu()
  clc
  disp('1.-Inecuación de primer grado con una incógnita');
  disp('2.-Inecuación de segundo grado con una incógnita');
  disp('3.-Inecuación con valor absoluto');
  disp('4.-Salir');
  p = input('Digite su opción: ')
endfunction
