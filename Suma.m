% Title        :Suma de números complejos
% Description  :Script para demostrar los tipos de axiomas
% Author       :Fátima Azucena MC
% Date         :14/02/2023

disp('Suma');
numeroRe1 = input('Digite el primer número del conjunto de los números reales (Real 1): ');
numeroRe2 = input('Digite el primer número del conjunto de los números reales (Real 2): ');
numeroIm1 = input('Digite el primer número del conjunto de los números reales (Imaginario 1): ');
numeroIm2 = input('Digite el primer número del conjunto de los números reales (Imaginario 1): ');
suma1 = ( numeroRe1 + numeroRe2 );
suma2 = ( numeroIm1 + numeroIm2 );
z1 = -3 - 5i;
z2 = 7 - 8i;
resum = z1 - z2;
reres = z1 - z2;
remul = z1 * z2;
rediv = z1 / z2;
disp('suma');
disp(resum);
fprintf("El resultado de la suma es: %s%d%s%d%s%d%d%s%d%d%s", "( " , numeroRe1 , " " ,  numeroRe2 , " ) + ( " , numeroIm1 , numeroIm2 , " ) = " , suma1 , suma2 , "i\n");

disp("")
disp("Resta")
numeroRe3 = input('Digite el primer número del conjunto de los números reales (Real 1): ');
numeroRe4 = input('Digite el primer número del conjunto de los números reales (Imaginario 1): ');
numeroIm3= input('Digite el primer número del conjunto de los números reales (Real 1): ');
numeroIm4 = input('Digite el primer número del conjunto de los números reales (Imaginario 2): ');
resta1 = ( numeroRe3 - numeroRe4 )
resta2 = ( numeroIm3 - numeroIm4 );
fprintf("El resultado de la resta es: %s%d%s%d%s%d%s%d%s%d%s%d%s", "( " , numeroRe3 , "  " ,  numeroIm3 , " ) + ( " , numeroRe4,"i " , numeroIm4, "i ) = " , resta2 , " " , resta3 , "i\n");
··̣··̣··





