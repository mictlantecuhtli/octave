
clear 
a = [ 9,1,1;1,2,1;1,18,1 ];
b = [ 1,1;1,1;1,1 ];
c = [ 10,2,2;2,3,2;2,19,2 ];
d = [ 18,5,7,8;18,3,4,4;9,1,1,1;9,2,3,4 ];
e = [ 9,2,2;27,7,7;18,5,4 ];
f = [ 9,2,2;28,8,8;18,5,4 ];
g = [ 19,5,7,8;18,4,4,4;9,1,2,1;9,2,3,4 ];
h = [ 1 + 3i,4-5i;8-9i,3+4i];
i = [ -1-1i,-2-2i;4+5i,6+7i];
j = [ -3,4;3,1 ];
k = [ 2,-1;3,4 ];
l = [3,-2;19,9];

disp("A");
disp(a);
disp("B");
disp(b);
disp("A*B");
resum = a*b;
disp(resum)
disp("***********************************************")
disp("B^t");
disp(b');
disp("A^t");
disp(a');
disp("B^t*A^t")
resum =(b')*(a');
disp(resum);
disp("***********************************************")
disp("A");
disp(a);
disp("C");
disp(c);
disp("A*C");
resum = a*c;
disp(resum);
disp("***********************************************")
disp("C");
disp(c);
disp("A");
disp(a);
disp("C*A")
resum = c*a;
disp(resum);
disp("***********************************************")
disp("|D|");
resp=det(d);
disp(resp);
disp("***********************************************")
disp("|E|");
resp=det(e);
disp(resp);
disp("***********************************************")
disp("|F|");
resp=det(f);
disp(resp);
disp("***********************************************")
disp("|G|");
resp=det(g);
disp(resp);
disp("***********************************************")
disp("h+i");
resp = h*i;
disp(resp);
disp("***********************************************")
resp = j*k;
disp(resp);
disp("***********************************************")
disp("|l|");
resp=inv(l);
disp(resp)
