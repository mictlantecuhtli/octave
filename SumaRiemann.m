% Fecha: 18-07-23
% Autor: Fátima Azucena MC
% Correo: fatimaazucenamartinez274@gmail.com
% Calcular y gráficar el área bajo la curva de
% la función establecida
% f(x) = 
% con un intervalo de: 

% Limpiar las variables y la pantalla
clc, clear
areatotal=0;

% Definimos el intervalo y el número de rectangulos
a = 1;
b = 5;
n = 100;

% Calcular la longuitud de la base de cada rectangulo
base = (b-a)/n;

% Definir la función 
x = a:base:(b-base);

