% Autor: Fatima Azucena MC
% Fecha: 20_03_2023
% Correo: fatimaazucenamartinez274@gmail.com

% Obtener el área bajo la curva de la siguiente función:
% además gráficala
% f(x) = x^3 + 3x + 1

clear
a = -3;
b = 4;
n = 100;

base_incremento = ( b - a ) / n;
Area = 0;
x = a;

for i=0:n-1
	x += base_incremento;
	f = (x.^3) + (3*x) + 1;
	Area = Area + abs(base_incremento*f);
endfor

disp(['El area aproximada es ', num2str(Area), ' unidades cuadradas']);

