% Crear figura
figure;

% Bucle para realizar la animación
for i = 1:100 % Cambia el número 100 según la duración deseada de la animación
    % Borrar figura anterior
    clf;

    % Posición del muñeco
    x = i * 0.1;
    y = 0;

    % Dibujar el muñeco
    % Cabeza
    plot(x, y, 'bo', 'MarkerSize', 40);
    hold on;

    % Cuerpo
    plot([x, x], [y-2, y-6], 'r-', 'LineWidth', 4);

    % Brazos
    plot([x-2, x+2], [y-3, y-3], 'g-', 'LineWidth', 3);

    % Piernas
    plot([x-1, x], [y-6, y-8], 'k-', 'LineWidth', 3);
    plot([x+1, x], [y-6, y-8], 'k-', 'LineWidth', 3);

    % Ajustar límites de los ejes
    axis([-10, 10, -10, 10]);

    % Título del fotograma
    title(sprintf('Fotograma %d', i));

    % Mostrar fotograma actual
    drawnow;

    % Pausa para crear el efecto de animación
    pause(0.1); % Ajusta el valor para controlar la velocidad de la animación
end

