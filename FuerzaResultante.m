% Autor: Fátima Azucena MC
% Fecha: 29_07_2023
% Correo: fatimaazucenamartinez274@gmail.com

% Realiza un código en octave en donde
% obtengas la fuerza resultante de 
% 2 vectores

theta_A = 60;  % Ángulo de A en grados
theta_B = 180;  % Ángulo de B en grados

theta_A_rad = deg2rad(theta_A);
theta_B_rad = deg2rad(theta_B);

A = 200;  % Magnitud de A en Newtons
B = 300;   % Magnitud de B en Newtons

A_x = A * cos(theta_A_rad);
A_y = A * sin(theta_A_rad);

B_x = B * cos(theta_B_rad);
B_y = B * sin(theta_B_rad);

R_x = A_x + B_x;
R_y = A_y + B_y;

R = sqrt(R_x^2 + R_y^2);

fprintf('La magnitud de la fuerza resultante es: %f N\n', R);

