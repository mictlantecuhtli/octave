% Vectores originales
v1 = [0; 4.5];
v2 = [0.5; 4];
v3 = [-0.5; 4];
v4 = [0; 3.5];
v5 = [0; 3];
v6 = [1.5; 3];
v7 = [-1.5; 3];
v8 = [0; 1.5];
v9 = [-1.5; 0];
v10 = [1.5; 0];

% Parámetros de traslación
dx = 2; % Traslación en el eje x
dy = 1; % Traslación en el eje y

% Crear figura
figure;

% Bucle para realizar la animación
for i = 1:100 % Cambia el número 100 según la duración deseada de la animación
    % Borrar figura anterior
    clf;

    % Calcular el vector de traslación para cada fotograma
    tx = dx * (i / 100);
    ty = dy * (i / 100);

    % Aplicar la transformación y traslación a los vectores
    v1_transformed = T * v1 + [tx; ty];
    v2_transformed = T * v2 + [tx; ty];
    v3_transformed = T * v3 + [tx; ty];
    v4_transformed = T * v4 + [tx; ty];
    v5_transformed = T * v5 + [tx; ty];
    v6_transformed = T * v6 + [tx; ty];
    v7_transformed = T * v7 + [tx; ty];
    v8_transformed = T * v8 + [tx; ty];
    v9_transformed = T * v9 + [tx; ty];
    v10_transformed = T * v10 + [tx; ty];

    % Graficar los vectores originales y transformados
    quiver(0, 0, v1(1), v1(2), 'b', 'LineWidth', 1.5);
    hold on;
    quiver(0, 0, v2(1), v2(2), 'r', 'LineWidth', 1.5);
    quiver(0, 0, v3(1), v3(2), 'g', 'LineWidth', 1.5);
    quiver(0, 0, v4(1), v4(2), 'm', 'LineWidth', 1.5);
    quiver(0, 0, v5(1), v5(2), 'c', 'LineWidth', 1.5);
    quiver(0, 0, v6(1), v6(2), 'k', 'LineWidth', 1.5);
    quiver(0, 0, v7(1), v7(2), 'y', 'LineWidth', 1.5);
    quiver(0, 0, v8(1), v8(2), 'b--', 'LineWidth', 1.5);
    quiver(0, 0, v9(1), v9(2), 'r--', 'LineWidth', 1.5);
    quiver(0, 0, v10(1), v10(2), 'g--', 'LineWidth', 1.5);
    quiver(tx, ty, v1_transformed(1), v1_transformed(2), 'b:', 'LineWidth', 1.5);
    quiver(tx, ty, v2_transformed(1), v2_transformed(2), 'r:', 'LineWidth', 1.5);
    quiver(tx, ty, v3_transformed(1), v3_transformed(2), 'g:', 'LineWidth', 1.5);
    quiver(tx, ty, v4_transformed(1), v4_transformed(2), 'm:', 'LineWidth', 1.5);
    quiver(tx, ty, v5_transformed(1), v5_transformed(2), 'c:', 'LineWidth', 1.5);
    quiver(tx, ty, v6_transformed(1), v6_transformed(2), 'k:', 'LineWidth', 1.5);
    quiver(tx, ty, v7_transformed(1), v7_transformed(2), 'y:', 'LineWidth', 1.5);
    quiver(tx, ty, v8_transformed(1), v8_transformed(2), 'b--', 'LineWidth', 1.5);
    quiver(tx, ty, v9_transformed(1), v9_transformed(2), 'r--', 'LineWidth', 1.5);
    quiver(tx, ty, v10_transformed(1), v10_transformed(2), 'g--', 'LineWidth', 1.5);
    axis([-10, 10, -10, 10]); % Ajusta los límites de los ejes según tus necesidades
    title(sprintf('Fotograma %d', i)); % Agrega un título con el número de fotograma actual

    % Mostrar fotograma actual
    drawnow;

    % Pausa para crear el efecto de animación
    pause(0.1); % Ajusta el valor para controlar la velocidad de la animación
end
