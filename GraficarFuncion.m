% Title        :Axiomas
% Description  :Script para demostrar los tipos de axiomas
% Author       :Fátima Azucena MC
% Date         :11/02/2023

% Limpiar
clear

x = linspace(-50,50);
y = 1.*x.*x - 8.*x + 15;
plot(x,y)
hold on;
grid()

rts = roots([1,-8,15]);
plot(rts, zeros(size(rts)), 'o', "color", "r")
