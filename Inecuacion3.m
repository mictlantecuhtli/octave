% Title        :Inecuacione
% Description  :Script para demostrar los tipos de axiomas
% Author       :Fátima Azucena MC
% Date         :11/02/2023

% Limpiar
clear

disp('')
disp('Inecuación con valor absoluto');
disp('Resolver la siguiente inecuación:')
disp('')
disp('| 6x - 11 | < 5');

syms x
y = expand( abs((6*x)-11) < 5 );
p = solve( abs((6*x)-11) < 5 );
disp('')
disp(y)
disp(' ')
disp(p)
disp('')
disp('By Honey (っ＾▿＾)')

