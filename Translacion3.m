% Datos originales
x = [1, 2, 3, 4, 5];
y = [2, 4, 1, 3, 5];

% Parámetros de traslación e inclinación
dx = 2; % Traslación en el eje x
dy = 1; % Traslación en el eje y
theta = pi/4; % Ángulo de inclinación

% Crear matriz de puntos originales
P = [x; y];

% Matriz de transformación
T = [cos(theta), -sin(theta); sin(theta), cos(theta)];

% Realizar traslación e inclinación
P_transformed = T * P + [dx; dy];

% Extraer las coordenadas transformadas
x_transformed = P_transformed(1, :);
y_transformed = P_transformed(2, :);

% Graficar los datos originales y transformados
plot(x, y, 'bo-', x_transformed, y_transformed, 'ro-');
legend('Datos originales', 'Datos transformados');
xlabel('Eje X');
ylabel('Eje Y');
