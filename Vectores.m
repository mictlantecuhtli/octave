% Vectores originales
v1 = [0; 4; 5];
v2 = [0.5; 4];
v3 = [-0.5; 4];
v4 = [0; 3.5];
v5 = [0; 3];
v6 = [1.5; 3];
v7 = [-1.5; 3];
v8 = [0; 1.5];
v9 = [-1.5; 0];
v10 = [1.5; 0];

% Crear figura
figure;

% Graficar los vectores
quiver3(0, 0, 0, v1(1), v1(2), v1(3), 'b', 'LineWidth', 1.5);
hold on;
quiver(0, 0, v2(1), v2(2), 'r', 'LineWidth', 1.5);
quiver(0, 0, v3(1), v3(2), 'g', 'LineWidth', 1.5);
quiver(0, 0, v4(1), v4(2), 'm', 'LineWidth', 1.5);
quiver(0, 0, v5(1), v5(2), 'c', 'LineWidth', 1.5);
quiver(0, 0, v6(1), v6(2), 'k', 'LineWidth', 1.5);
quiver(0, 0, v7(1), v7(2), 'y', 'LineWidth', 1.5);
quiver(0, 0, v8(1), v8(2), 'b--', 'LineWidth', 1.5);
quiver(0, 0, v9(1), v9(2), 'r--', 'LineWidth', 1.5);
quiver(0, 0, v10(1), v10(2), 'g--', 'LineWidth', 1.5);

% Ajustar los límites de los ejes
axis([-2, 2, -2, 5, 0, 6]); % Ajusta los límites de los ejes según tus necesidades

% Agregar etiquetas a los ejes
xlabel('Eje X');
ylabel('Eje Y');
zlabel('Eje Z');

% Mostrar la cuadrícula
grid on;

% Mostrar la leyenda
legend('v1', 'v2', 'v3', 'v4', 'v5', 'v6', 'v7', 'v8', 'v9', 'v10');

% Establecer el aspecto de igual escala en los ejes
daspect([1 1 1]);

% Ajustar la vista 3D
%view(30, 30);
