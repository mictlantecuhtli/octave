% Title        :Inecuacione
% Description  :Script para demostrar los tipos de axiomas
% Author       :Fátima Azucena MC
% Date         :11/02/2023

% Limpiar
clear

disp('')
disp('Inecuación de primer grado con una incógnita');
disp('Resolver la siguiente inecuación:') 
disp('')
disp('2x + 3 + 2 ( x + 1 ) < - 3 ( 1 – x )');

syms x
r = -20:0.1:20;
y = expand((4 * x) + 3 + 2 * ( x + 1 ) < (- 3 * ( 1 - x)));
p = solve((4 * x) + 3 + 2 * ( x + 1 ) < (- 3 * ( 1 - x)));
disp('')
disp(y)
disp(' ')
disp(p)
disp('')

disp('By Honey (っ＾▿＾)')

