% Autor: Fatima Azucena MC
% Fecha: 27_03_23
% Correo: fatimaazucenamartinez274@gmail.com

clear

a = [ 1,3,2;7,6,4;45,-8,17 ];
b = [ 1,0,0;0,1,0;0,0,1 ];
c = [ 3,3,3;3,3,3;2,2,2 ];

disp("***********************************************")
disp("De acuerdo a las siguientes matrices: ");
disp("A");
disp(a);
disp("B");
disp(b);
disp("C");
disp(c);
disp("Realice las siguientes operaciones");
disp("***********************************************")
disp("A + ( B / C )");
resum1 = (b/c);
resum2 = a+resum1;
disp(resum2);
disp("***********************************************")
disp("C - A ");
resum = c-a;
disp(resum);
disp("***********************************************")
disp("(A)(B)(C)");
resum = ((a)*(b)*(c));
disp(resum);
disp("***********************************************")
