% Title        :Axiomas
% Description  :Script para demostrar los tipos de axiomas
% Author       :Fátima Azucena MC
% Date         :11/02/2023


% Limpiar variables
clear
% Asignación de variables
a = 2
b = 4
c = 6

a1 = a + b;
a2 = a * b;
a3 = a + b;
a4 = b + a;
a5 = a * b;
a6 = b * a;
a7 = a + ( b + c );
a8 = ( a + b ) + c;
a9 = a * ( b * c );
a10 = (a*b)*c;
a11 = a + 0;
a12 = a * 1;
a13 = a + ( - a);
a14 = a * ( 1 / a );
a15 = a * ( b + c );
a16 = a * b + a * c;

fprintf('\n\nAxioma de cerradura\n')
fprintf('Adición (a+b∈R): %d%s%d%s%d', a , ' + ', b , ' = ' , a1)
fprintf('\nProducto (a⋅b∈R): %d%s%d%s%d', a , ' * ', b , ' = ' , a2)

fprintf('\n\nAxioma conmutativo\n')
fprintf('Adición ( a+b=b+a ): %d%s%d%s%d%s%d%s%d%s%d:', a , ' + ', b , ' = ' , a3 , '  o  ', b , ' + ', a , ' = ' , a4)
fprintf('\nProducto ( a⋅b=b⋅a ): %d%s%d%s%d%s%d%s%d%s%d:', a , ' * ', b , ' = ' , a5 , '  o  ', b , ' * ', a , ' = ' , a6)

fprintf('\n\nAxioma asociativo\n')
fprintf('Adición ( a+(b+c)=(a+b)+c ): %d%s%d%s%d%s%d%s%s%d%s%d%s%d%s%d:', a , ' + ( ', b , ' + ' , c , ' ) = ', a7 , '  o  ', ' ( ', a , ' + ', b , ' ) + ', c , ' = ', a8 )
fprintf('\nProducto ( a(b⋅c)=(a⋅b)c ): %d%s%d%s%d%s%d%s%s%d%s%d%s%d%s%d', a , ' * ( ', b , ' * ' , c , ' ) = ', a9 , '  o  ', ' ( ', a , ' * ', b , ' ) * ', c , ' = ', a10 )

fprintf('\n\nAxioma elemento neutro\n')
fprintf('Adición ( a+0=a ): %d%s%d', a , ' + 0 = ', a11)
fprintf('\nProducto ( a*1=a ): %d%s%d', a , ' * 1 = ', a12)

fprintf('\n\nAxioma inverso\n')
fprintf('Adición ( a+(−a)=0 ): %d%s%d%s%d', a , ' + ( - ', a , ' ) = ', a13 )
fprintf('\nProducto ( a⋅(1/a)=1 ): %d%s%d%s%d', a , ' * ( 1 / ' , a , ' ) = ' , a14  )

fprintf('\n\nAxioma distributivo\n')
fprintf('Adición/Producto ( a(b+c)=a⋅b+a⋅c ): %d%s%d%s%d%s%d%s%d%s%d%s%d%s%d', a , ' * ( ' , b , ' + ', c , ' ) = ' , a , ' * ' , b , ' + ', a , ' * ', c , ' = ' , a15)
fprintf('\n')



