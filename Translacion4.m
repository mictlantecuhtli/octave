% Datos originales
x = [1, 2, 3, 4, 5];
y = [2, 4, 1, 3, 5];

% Parámetros de traslación e inclinación
dx = 2; % Traslación en el eje x
dy = 1; % Traslación en el eje y
theta = pi/6; % Ángulo de inclinación

% Crear figura
figure;

% Bucle para realizar la animación
for i = 1:100 % Cambia el número 100 según la duración deseada de la animación
    % Borrar figura anterior
    clf;

    % Calcular el ángulo de inclinación para cada fotograma
    if i <= 50
        % Translación hacia la derecha
        angle = theta;
    else
        % Translación hacia la izquierda
        angle = -theta;
    end

    % Matriz de transformación
    T = [cos(angle), -sin(angle); sin(angle), cos(angle)];

    % Realizar traslación e inclinación
    P_transformed = T * [x; y] + [dx; dy];

    % Extraer las coordenadas transformadas
    x_transformed = P_transformed(1, :);
    y_transformed = P_transformed(2, :);

    % Graficar los datos transformados
    plot(x_transformed, y_transformed, 'bo-');
    axis([-10, 10, -10, 10]); % Ajusta los límites de los ejes según tus necesidades
    title(sprintf('Fotograma %d', i)); % Agrega un título con el número de fotograma actual

    % Mostrar fotograma actual
    drawnow;

    % Pausa para crear el efecto de animación
    pause(0.1); % Ajusta el valor para controlar la velocidad de la animación
end
