% Autor: Fatima Azucena MC
% Fecha: 27_03_23
% Correo: fatimaazucenamartinez274@gmail.com

clear

a = [20,-10,8,5;12,25,33,12;1,9,2,8;-10,-9,-15,-40];

disp("***********************************************")
disp("Dada la siguiente matrice: ");
disp(a);
disp("Obtener la matriz inversa, transpuesta y adjunta");
disp("***********************************************")
disp("Inversa");
resp = inv(a);
disp(resp);
disp("Transpuesta");
resp = (a');
disp(resp);
disp("Adjunta");

