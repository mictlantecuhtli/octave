% Title        :Axiomas
% Description  :Script para demostrar los tipos de axiomas
% Author       :Fátima Azucena MC
% Date         :11/02/2023

% Limpiar
clear

% Ejemplo mínimo de material para envase cilíndrico
% Rango de 0..1 en i = 0.1
r=-20:0.1:20;
% Valor de la función
ar =@(r) (pi() * (r.^2)) + (2./(r));
% Función a plotear
y = (pi() * (r.^2)) + (2./(r));
% Función para determinar valor mínimo
resr = fminbnd(ar,0,1);
% Dibujar r,y
plot(r, y);
% Título
title(['Mínimo material r * ' num2str(resr)]);
% Etiqueta para x
xlabel(['Min r * ' num2str(resr)]);
% Etiqueta para y
ylabel(['Min y * ' num2str( ar(resr))]);

