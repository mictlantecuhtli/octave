% Datos originales
x = [, 2, 3, 4, 5];
y = [2, 4, 1, 3, 5];

% Valor de traslación
dx = 0.1; % Traslación en el eje x por fotograma
dy = 0.1; % Traslación en el eje y por fotograma

% Crear figura
figure;

% Bucle para crear la animación
for i = 1:100 % Cambia el número 100 según la duración deseada de la animación
    % Aplicar traslación
    x_translated = x + i*dx;
    y_translated = y + i*dy;

    % Borrar figura anterior
    clf;

    % Graficar datos trasladados
    plot(x_translated, y_translated, 'b-o');
    axis([0, 10, 0, 10]); % Ajusta los límites de los ejes según tus necesidades
    title(sprintf('Fotograma %d', i)); % Agrega un título con el número de fotograma actual

    % Mostrar fotograma actual
    drawnow;

    % Pausa para crear el efecto de animación
    pause(0.1); % Ajusta el valor para controlar la velocidad de la animación
end
